{ config, pkgs, ... }:

let
  gnome-subtitles = pkgs.callPackage ./gnome-subtitles {};
  bitwig' = pkgs.callPackage ./bitwig-studio { };
in
{
  imports =
    [
      ./cachix.nix
      ./hardware-configuration.nix
    ];

  boot = {
    kernelModules = [ "kvm-amd" "kvm-intel" "snd-seq" "snd-rawmidi" ];
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
  };

  hardware = {
    opengl.driSupport32Bit = true;
    pulseaudio.enable = true;
  };

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    package = pkgs.nixUnstable;
    trustedUsers = [ "root" "lc" ];
  };

  services = {
    flatpak.enable = true;
    jack = {
      alsa.enable = false;
      jackd.enable = true;
      loopback.enable = true;
    };
    logind.extraConfig = ''
      RuntimeDirectorySize=10G
      '';
    postgresql.enable = true;
    xserver = {
      enable = true;
      exportConfiguration = true;
      desktopManager.gnome3.enable = true;
      displayManager.lightdm.enable = true;
      layout = "us,fr,de";
      videoDrivers = [ "nvidia" ];
      wacom.enable = true;
      windowManager.i3 = {
        enable = true;
        extraPackages = with pkgs; [dmenu i3status i3lock i3blocks ];
      };
      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
      };
      xkbOptions = "eurosign:e, compose:ralt, grp:alt_space_toggle";
    };
  };

  swapDevices = [ { device = "/var/swap"; size = 16384; } ];

  nixpkgs = {
    config =
    { allowUnfree = true;
      allowUnsupportedSystem = true;
    };
  };

  networking.hostName = "aiur";


  programs = {
    adb.enable = true;
    dconf.enable = true;
  };

  fonts.fonts = import ./fonts.nix { pkgs = pkgs; };

  sound.enable = true;

  virtualisation.libvirtd.enable = true;

  environment.systemPackages = with pkgs; [
    alacritty
    android-studio
    anki
    audacity
    autoconf
    automake
    bash
    bfg-repo-cleaner
    bitwig'
    binutils
    bitwig-studio
    blender
    brave
    busybox
    cabal-install
    cachix
    cadence
    chez
    citrix_workspace_20_12_0
    cudatoolkit_10_2
    dconf
    dhall
    direnv
    discord
    dolphinEmu
    dmenu
    dos2unix
    file
    firefox
    ffmpeg
    foldingathome
    gcc
    ghc
    ghcid
    gimp
    git
    git-lfs
    gnumake
    gnupg
    gnome-subtitles
    google-chrome
    gst_all_1.gstreamer
    gst_all_1.gstreamer.dev
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-ugly
    gst_all_1.gst-libav
    #hashlink
    haskell-language-server
    haskellPackages.Agda
    haskellPackages.alex
    haskellPackages.happy
    haskellPackages.pandoc
    haskellPackages.wai-app-static
    haxe
    hledger
    hlint
    idris2
    imagemagick
    jq
    keybase
    krita
    libreoffice
    lutris
    mkpasswd
    mkvtoolnix
    ncurses
    neko
    neovim
    niv
    nix-prefetch-git
    rxvt_unicode
    obs-studio
    obsidian
    ormolu
    owncloud-client
    pavucontrol
    pijul
    piper
    projectm
    python3
    qjackctl
    quasselClient
    SDL2
    scrot
    slack
    stack
    steam
    stylish-haskell
    sqlite
    teams
    terminator
    thunderbird
    tor-browser-bundle-bin
    transmission-gtk
    unzip
    vim
    virt-manager
    vlc
    vulkan-loader
    vulkan-headers
    vulkan-tools
    wget
    wineWowPackages.staging
    xbindkeys
    xdotool
    xautomation
    xorg.xev
    youtube-dl
    zsh
    zlib
  ];

  users.users = {
    lc = {
      extraGroups = [ "adbusers" "audio" "jackaudio" "libvirtd" "wheel"];
      hashedPassword = "$6$gPzhjvk1TIy$kJpFfg/ZoHJ363REctU3b11D3CXqAEC5MXplsy9cWlyvVaVmEASdZUEWHVO6dKdn7gFrxNwrb4DiR/JrYUxZc1";
      isNormalUser = true;
      shell = pkgs.fish;
    };
  };

  system.stateVersion = "20.09";

}
