
{
  nix = {
    binaryCaches = [
      "https://shakebook.cachix.org"
    ];
    binaryCachePublicKeys = [
      "shakebook.cachix.org-1:y42OvYap/3mM35QNli/aqCr8PuxkWYZg+zjvhyz+Enc="
    ];
  };
}
    