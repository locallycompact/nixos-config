
{
  nix = {
    binaryCaches = [
      "https://locallycompact.cachix.org"
    ];
    binaryCachePublicKeys = [
      "locallycompact.cachix.org-1:wITqfJEAQfaImITypwQhGxlkoLiTlCGk6B+fSm/xXxc="
    ];
  };
}
    