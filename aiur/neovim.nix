self: super: {
  neovim = super.neovim.override {
    configure = {
      customRC = ''
        set expandtab
        set shiftwidth=2
        set softtabstop=2
        set bg=light
        set undofile
        set ignorecase
        set smartcase
        set mouse-=a
        set backspace=2
        autocmd BufWritePre * %s/\s\+$//e
        map <C-p> :Files<CR>
        map <C-a> :Ag<CR>
        map <C-n> :NERDTreeToggle<CR>
        map <C-t> :TagbarToggle<CR>
        let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
        tnoremap <Esc> <C-\><C-n>
        let g:LanguageClient_serverCommands = { 'haskell': ['haskell-language-server-wrapper', '--lsp'] }
        nnoremap <F5> :call LanguageClient_contextMenu()<CR>
        let g:LanguageClient_rootMarkers = ['*.cabal', 'stack.yaml']
        map <Leader>lk :call LanguageClient#textDocument_hover()<CR>
        map <Leader>lg :call LanguageClient#textDocument_definition()<CR>
        map <Leader>lr :call LanguageClient#textDocument_rename()<CR>
        map <Leader>lf :call LanguageClient#textDocument_formatting()<CR>
        map <Leader>lb :call LanguageClient#textDocument_references()<CR>
        map <Leader>la :call LanguageClient#textDocument_codeAction()<CR>
        map <Leader>ls :call LanguageClient#textDocument_documentSymbol()<CR>
      '';
      packages.myPackages = with self.vimPlugins; {
        start = [ LanguageClient-neovim vim-nix dhall-vim idris-vim];
      };
    };
  };
}
